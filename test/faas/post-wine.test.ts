import PostWine = require('../../src/faas/post-wine');

import AWSMock = require('aws-sdk-mock');
import AWS_SDK = require('aws-sdk');
import {sandbox} from 'sinon';
import {expect} from '../support/expect';
import {APIGatewayEvent} from "aws-lambda";

AWSMock.setSDKInstance(AWS_SDK);

describe('Post Wine Lambda', () => {
    let sb;

    beforeEach(() => {
        sb = sandbox.create();
        AWSMock.restore('DynamoDB.DocumentClient');
    });

    afterEach(() => {
        sb.restore();
    });

    let event = {};
    const context: any = {};

    it('should post a wine', () => {
        const awsmock = AWSMock.mock('DynamoDB.DocumentClient', 'put', function(params, callback) {
            callback(null, 'successfully put item in database');
        });
        event = {
            body: '{\n "name": "Merlot",\n "description": "description",\n "year": 2015\n }',
        };
        PostWine.handle(event as APIGatewayEvent, context, (err, data) => {
            expect(awsmock.stub.calledOnce).to.be.true;
            expect(data.statusCode).to.equal(200);
            expect(data.body).to.equal(JSON.stringify("{ 'succes' : 'true' }"));
        });
    });

    it('should return a conflict if item already exists', () => {
        const error = {
            message: 'Connection refused due to timeout. Response took too long.',
        };
        const awsmock = AWSMock.mock('DynamoDB.DocumentClient', 'put', function(params, callback) {
            callback(error);
        });
        event = {
            body: '{\n "name": "Merlot",\n "description": "description",\n "year": 2015\n }',
        };
        PostWine.handle(event as APIGatewayEvent, context, (err, data) => {
            expect(awsmock.stub.calledOnce).to.be.true;
            expect(data.statusCode).to.equal(409);
            expect(data.body).to.equal('Connection refused due to timeout. Response took too long.');
        });
    });

    it('should return Bad Request when Post body is not valid', () => {
        const awsmock = AWSMock.mock('DynamoDB.DocumentClient', 'put', function(params, callback) {
            callback(null, 'successfully put item in database');
        });
        event = {
            body: '{\n "description": "description",\n "year": 2015\n }',
        };
        PostWine.handle(event as APIGatewayEvent, context, (err, data) => {
            expect(awsmock.stub.called).to.be.false;
            expect(data.statusCode).to.equal(400);
            expect(data.body).to.equal('Name should be defined');
        });
    });
});
