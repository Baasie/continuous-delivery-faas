resource "aws_dynamodb_table" "dynamodb_wine_record_table" {
  name             = "cdfaas-WineRecord-${var.stage}"
  read_capacity    = "10"
  write_capacity   = "10"
  hash_key         = "uuid"

  attribute {
    name = "uuid"
    type = "S"
  }
}

output "dynamodb_wine_record_table" {
  value = "${aws_dynamodb_table.dynamodb_wine_record_table.arn}"
}