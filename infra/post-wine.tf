data "archive_file" "post_wine_lambda" {
  type = "zip"
  source_file = "../dist/post-wine.js"
  output_path = "../dist/post-wine.zip"
}

resource "aws_lambda_function" "post_wine_lambda" {
  function_name = "cdfaas-post_wine-${var.stage}"
  handler = "post-wine.handle"
  runtime = "nodejs8.10"
  filename = "${data.archive_file.post_wine_lambda.output_path}"
  source_code_hash = "${base64sha256(file("${data.archive_file.post_wine_lambda.output_path}"))}"
  role = "${aws_iam_role.lambda_exec_role.arn}"
  environment {
    variables = {
      DYNAMO_WINE_TABLE_NAME = "cdfaas-WineRecord-${var.stage}"
    }
  }
}

resource "aws_api_gateway_method" "post_wine_api_method" {
  rest_api_id = "${aws_api_gateway_rest_api.in_vino_veritas_api.id}"
  resource_id = "${aws_api_gateway_resource.wines_resource.id}"
  http_method = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "post_wine_method-integration" {
  rest_api_id = "${aws_api_gateway_rest_api.in_vino_veritas_api.id}"
  resource_id = "${aws_api_gateway_resource.wines_resource.id}"
  http_method = "${aws_api_gateway_method.post_wine_api_method.http_method}"
  type = "AWS_PROXY"
  uri = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.region}:${var.account_id}:function:${aws_lambda_function.post_wine_lambda.function_name}/invocations"
  integration_http_method = "POST"
  depends_on = ["aws_lambda_function.post_wine_lambda"]
}

/**
 * Allow APIGateway to access the Website Lambda Function.
 */
resource "aws_lambda_permission" "post_wine_SeoWebsiteApiGateway" {
  depends_on = [
    "aws_lambda_function.post_wine_lambda",
    "aws_api_gateway_rest_api.in_vino_veritas_api",
    "aws_api_gateway_method.get_wines_api_method"
  ]
  statement_id = "AllowExecutionFromAPIGateway"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.post_wine_lambda.function_name}"
  principal = "apigateway.amazonaws.com"
}