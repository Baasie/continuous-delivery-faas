variable "region" {}
variable "account_id" {}
variable "app_name" {}
variable "stage" {}

provider "aws" {
  region  = "${var.region}"
}

terraform {
  backend "local" {
    path = "./state/terraform.tfstate"
  }
  required_version = "= 0.11.7"
}

resource "aws_api_gateway_rest_api" "in_vino_veritas_api" {
  name = "cdfaas-${var.stage}"
  description = "Api for In Vino Veritas"
}

resource "aws_api_gateway_resource" "wines_resource" {
  rest_api_id = "${aws_api_gateway_rest_api.in_vino_veritas_api.id}"
  parent_id = "${aws_api_gateway_rest_api.in_vino_veritas_api.root_resource_id}"
  path_part = "wines"
}

resource "aws_api_gateway_deployment" "in_vino_veritas_deployment_aat" {
  depends_on = [
    "aws_api_gateway_method.get_wines_api_method",
    "aws_api_gateway_integration.get_wines_method-integration",
    "aws_api_gateway_method.post_wine_api_method",
    "aws_api_gateway_integration.post_wine_method-integration"
  ]
  rest_api_id = "${aws_api_gateway_rest_api.in_vino_veritas_api.id}"
  stage_name = "${var.stage}"
}

output "api_url" {
  value = "https://${aws_api_gateway_rest_api.in_vino_veritas_api.id}.execute-api.${var.region}.amazonaws.com/${aws_api_gateway_deployment.in_vino_veritas_deployment_aat.stage_name}"
}