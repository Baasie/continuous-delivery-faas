

resource "aws_iam_role" "lambda_exec_role" {
  name_prefix = "lambda_exec_role"
  assume_role_policy = "${file("./iam/lambda-role.json")}"
}

resource "aws_iam_role_policy" "lambda_read_write" {
  name = "in_vino_veritas_lambda_read_write"
  role = "${aws_iam_role.lambda_exec_role.id}"

  policy = "${file("./iam/lambda-policy.json")}"
}