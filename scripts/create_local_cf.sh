#!/usr/bin/env bash
set -e -u

npm run test
npm run build

aws cloudformation package --region eu-west-1 --template-file template.yaml --s3-bucket baasie.cd-faas.release --output-template-file packaged.yaml
aws cloudformation deploy --region eu-west-1 --template-file ./packaged.yaml --stack-name cdfaas-kb-local --parameter-overrides Stage=kb-cf-local --capabilities CAPABILITY_IAM