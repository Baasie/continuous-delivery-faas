#!/usr/bin/env bash
set -e -u

aws cloudformation delete-stack --region eu-west-1 --stack-name cdfaas-kb-local