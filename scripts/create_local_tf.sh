#!/usr/bin/env bash
set -e -u

source ./scripts/local_variables.sh

npm run test
npm run build

cd infra
terraform plan -var account_id=${AWS_ACCOUNT_ID} -var region=${AWS_REGION} -var stage=${aws_stage}
terraform apply -auto-approve -var account_id=${AWS_ACCOUNT_ID} -var region=${AWS_REGION} -var stage=${aws_stage}


pwd
sed  -i -e "s^export API_URL=.*^export API_URL=$(terraform output api_url)^" ../scripts/local_variables.sh

