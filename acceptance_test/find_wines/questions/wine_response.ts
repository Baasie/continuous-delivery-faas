import {expect} from '../../support/expect';

export const isNotEmpty = () => axiosResponse => axiosResponse.then(lastResponse =>
    expect(lastResponse.data).to.not.be.empty,
);
export const isWine = (expected: string) => axiosResponse => axiosResponse.then(lastResponse =>
    expect(lastResponse.data[0].name).to.equal(expected),
);
