import {Interaction, PerformsTasks, Task} from '@serenity-js/core/lib/screenplay';
import {Get} from '@serenity-js/rest';
import {Wine} from '../../../src/domain/wine';
import {Properties} from '../../properties';
import {Insert} from '../../support/interactions/dynamo/insert_item';
import {delay} from "../../../features/support/delay";

export class Wines implements Task {

    private interactions: Interaction[] = [];

    static add(wines: Wine[]) {
        return new Wines(wines);
    }

    performAs(actor: PerformsTasks): PromiseLike<void> {
        return actor.attemptsTo(
            ...this.interactions,
        );
    }

    constructor(wines: Wine[]) {
        wines.forEach(wine => this.interactions.push(Insert.itemIntoTable(wine.toJSON(), Properties.WINE_TABLE_NAME)));
    }

    toString = () => `{0} Adds wines`;

}

export const GetWines = () => Task.where(`#actor wants to get Wines`,
    Get.resource('/wines'),
);
