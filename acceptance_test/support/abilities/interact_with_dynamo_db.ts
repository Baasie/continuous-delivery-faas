import {Ability, UsesAbilities} from '@serenity-js/core/lib/screenplay';
import {DynamoDB} from 'aws-sdk';

export class InteractWithDynamoDb implements Ability {

    /**
     *
     * Instantiates the Ability to BrowseTheWeb, allowing the Actor to interact with a Web UI
     *
     * @param dynamoDb
     * @return {BrowseTheWeb}
     */
    static using(dynamoDb: DynamoDB.DocumentClient) {
        return new InteractWithDynamoDb(dynamoDb);
    }

    /**
     * Used to access the Actor's ability to BrowseTheWeb from within the Interaction classes, such as Click or Enter
     *
     * @param actor
     * @return {BrowseTheWeb}
     */
    static as(actor: UsesAbilities): InteractWithDynamoDb {
        return actor.abilityTo(InteractWithDynamoDb);
    }

    constructor(private dynamoDb: DynamoDB.DocumentClient) {
    }

    put(params: any): PromiseLike<any> {
        return this.dynamoDb.put(params).promise().then(function(data) {
            return Promise.resolve(data);
        }, function(err) {
            return Promise.reject(err);
        });
    }

    get(params: any): PromiseLike<any> {
        return this.dynamoDb.scan(params).promise().then(function(data) {
            return Promise.resolve(data.Items);
        }, function(err) {
            return Promise.reject(err);
        });
    }
}
