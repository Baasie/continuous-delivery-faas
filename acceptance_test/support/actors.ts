import {Actor} from '@serenity-js/core/lib/screenplay';
import {Cast} from '@serenity-js/core/lib/stage';
import {Properties} from '../properties';
import {CallAnApi} from '@serenity-js/rest';
import {InteractWithDynamoDb} from './abilities/interact_with_dynamo_db';
import {DynamoDb} from './dynamo_db';
import axios, {AxiosInstance} from 'axios';


export class Actors implements Cast {

    actor(name: string): Actor {
        switch (name) {
            case 'Admin':
                console.log('InteractWithDynamoDb on Table: ' + Properties.WINE_TABLE_NAME);
                return Actor.named(name).whoCan(InteractWithDynamoDb.using(DynamoDb.DocumentClient()));
            default:
                const axiosInstance: AxiosInstance = axios.create({
                    baseURL: Properties.END_POINT,
                    timeout: 4000,
                    headers: {Accept: 'application/json,application/xml'},
                });
                console.log('CallAnApi on End Points: ' + Properties.END_POINT);
                return Actor.named(name).whoCan(CallAnApi.using(axiosInstance));
        }
    }
}
