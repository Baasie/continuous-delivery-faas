import {Interaction, UsesAbilities} from '@serenity-js/core/lib/screenplay';
import {InteractWithDynamoDb} from '../../abilities/interact_with_dynamo_db';

export class Insert implements Interaction {

    private params: any;

    static itemIntoTable(item: any, tableName: string) {
        return new Insert(tableName, item);
    }

    performAs(actor: UsesAbilities): PromiseLike<any> {
        return InteractWithDynamoDb.as(actor).put(this.params);
    }

    constructor(tableName: string, item: any ) {
        this.params = {
            TableName: tableName,
            Item: item,
        };
    }

    toString = () => `{0} enters item into table : ${this.params.TableName}`;
}
