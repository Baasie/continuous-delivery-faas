import {DynamoDB} from 'aws-sdk';

export const DynamoDb = {
    DocumentClient: () => {
        return new DynamoDB.DocumentClient({region: 'eu-west-1'});
    },

};
