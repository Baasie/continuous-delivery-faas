import {Task} from '@serenity-js/core/lib/screenplay';
import {Wine} from '../../../src/domain/wine';
import {Post} from '@serenity-js/rest';

export const RegisterWine = (wine: Wine) => Task.where(`#actor wants to register a Wine`,
    Post.item(wine).on('/wines'),
);
