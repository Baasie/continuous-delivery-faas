import {Question} from '@serenity-js/core/lib/screenplay';
import {Properties} from '../../properties';
import {InteractWithDynamoDb} from '../../support/abilities/interact_with_dynamo_db';
import {expect} from '../../support/expect';

const params = {
    TableName: Properties.WINE_TABLE_NAME,
};

export const TableItems = () => Question.about(`The Message`, actor =>
    InteractWithDynamoDb.as(actor).get(params) as PromiseLike<any>,
);

export const isWine = (expected: string) => dynamoPromise => dynamoPromise.then(
    row => expect(row[0].name).to.equal(expected),
);
