# Continuous Delivery FaaS #

[![build status](https://gitlab.com/Baasie/continuous-delivery-faas/badges/master/build.svg)](https://gitlab.com/Baasie/continuous-delivery-faas/commits/master)

Slides of my talk at codemotion: https://speakerdeck.com/baasie/continuous-delivering-serverless-applications-in-the-pipeline-indispensable-advice-from-the-trenches

## Javascript reports ##
[javascript coverage](https://baasie.gitlab.io/continuous-delivery-faas/coverage/)   
[javascript mutation](https://baasie.gitlab.io/continuous-delivery-faas/mutation/)   
[BDD Report](https://baasie.gitlab.io/continuous-delivery-faas/bdd/)


## setup ##
Create a script for local usage under scripts  called local_variables.sh with the follow details:
```
#!/usr/bin/env bash
export AWS_ACCOUNT_ID=""  
export AWS_REGION=""  
export aws_stage=""  
export API_URL=  
```

be sure to have aws sdk installed and set your profile  
use npm i for the rest

## usage ##
```
npm run local:tf   ->  deploy terraform script to aws
source ./scripts/local_variables.sh     -> set the output of the api on your command line
npm run aat   ->  Run acceptance tests
npm run local:tf:destroy  -> Destroy terraform script from aws
```
