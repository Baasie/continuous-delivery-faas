#!/bin/bash
set -e -u

SCRIPT_DIR=$PWD/ci

echo "npm Version:"
npm -v

echo "npm install"
npm i --silent

echo "run tests"
npm run test

echo "run build"
npm run build

echo "Zipping dist to ${RELEASE_NAME}.zip"
mkdir -p release

cp ./template.yaml ./dist/template.yaml
cp -R infra dist/
cp $SCRIPT_DIR/backend_override.tf ./dist/infra/backend_override.tf
cd dist
zip -r ./../release/release.zip .
cd ..

echo "Uploading zip to releases artifactory"
aws s3 cp ./release/release.zip s3://${RELEASE_BUCKET}/${RELEASE_NAME}.zip