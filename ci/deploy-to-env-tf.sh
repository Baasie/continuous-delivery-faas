#!/bin/bash
set -e -u

SCRIPT_DIR=$PWD/ci

echo "Deploying infra on ${AWS_REGION}, tf-state in bucket: ${tf_state_bucket} & folder: ${tf_state_folder}"

mkdir -p dist
aws s3 cp s3://${RELEASE_BUCKET}/${RELEASE_NAME}.zip ./dist/${RELEASE_NAME}.zip
unzip -o ./dist/${RELEASE_NAME}.zip -d ./dist

terraform --version

cp $SCRIPT_DIR/backend_override.tf ./infra/backend_override.tf

cd infra
terraform get

terraform init \
    -backend-config="bucket=${tf_state_bucket}" \
    -backend-config="key=${tf_state_folder}/terraform.tfstate" \
    -backend-config="region=${AWS_REGION}"

# Create required resources specified in terraform/main.tf if they do not exist
terraform apply -auto-approve -var account_id=${AWS_ACCOUNT_ID} -var region=${AWS_REGION} -var stage=${aws_stage}

export API_URL=$(terraform output api_url)
echo "API_URL ${API_URL}"
cd ..