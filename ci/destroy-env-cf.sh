#!/usr/bin/env bash
set -e -u

SCRIPT_DIR=$PWD/ci

mkdir -p dist
aws s3 cp s3://${RELEASE_BUCKET}/${RELEASE_NAME}.zip ./dist/${RELEASE_NAME}.zip
unzip -o ./dist/${RELEASE_NAME}.zip -d ./dist

echo "Destroying infra on ${AWS_REGION}"
aws cloudformation delete-stack --region ${AWS_REGION} --stack-name cdfaas-${aws_stage}