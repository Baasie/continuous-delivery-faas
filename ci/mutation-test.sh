#!/usr/bin/env bash
set -e -u

SCRIPT_DIR=$PWD/ci

echo "npm Version:"
npm -v

echo "npm install"
npm i --silent

echo "Mutation testing"
npm run test:mutation