#!/bin/bash
set -e -u

SCRIPT_DIR=$PWD/ci

echo "Destroying infra on ${AWS_REGION}, tf-state in bucket: ${tf_state_bucket} & folder: ${tf_state_folder}"

mkdir -p dist
aws s3 cp s3://${RELEASE_BUCKET}/${RELEASE_NAME}.zip ./dist/${RELEASE_NAME}.zip
unzip -o ./dist/${RELEASE_NAME}.zip -d ./dist

cd infra
terraform get

terraform init \
    -backend-config="bucket=${tf_state_bucket}" \
    -backend-config="key=${tf_state_folder}/terraform.tfstate" \
    -backend-config="region=${AWS_REGION}"

terraform destroy --force -var account_id=${AWS_ACCOUNT_ID} -var region=${AWS_REGION} -var stage=${aws_stage}

cd ..

