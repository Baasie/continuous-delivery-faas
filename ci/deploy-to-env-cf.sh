#!/bin/bash
set -e -u

SCRIPT_DIR=$PWD/ci

mkdir -p dist
aws s3 cp s3://${RELEASE_BUCKET}/${RELEASE_NAME}.zip ./dist/${RELEASE_NAME}.zip
unzip -o ./dist/${RELEASE_NAME}.zip -d ./dist

echo "Deploying infra on ${AWS_REGION}"

aws cloudformation package --region ${AWS_REGION} --template-file template.yaml --s3-bucket baasie.cd-faas.release --output-template-file packaged.yaml
aws cloudformation deploy --region ${AWS_REGION} --template-file ./packaged.yaml --stack-name cdfaas-${aws_stage} --parameter-overrides Stage=${aws_stage} --capabilities CAPABILITY_IAM