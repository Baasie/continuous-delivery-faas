import {Serialised, TinyType, TinyTypeOf} from 'tiny-types';
import { v4 as uuid } from 'uuid';

export class Name extends TinyTypeOf<string>() {
    static fromJSON = (name: string) => new Name(name);
}

export class Description extends TinyTypeOf<string>() {
    static fromJSON = (description: string) => new Description(description);
}

export class Year extends TinyTypeOf<number>() {
    static fromJSON = (year: number) => new Year(year);
}

export class UUID extends TinyTypeOf<string>() {
    static new = () => new UUID(uuid());
}

export class Wine extends TinyType {
    private uuid: UUID;

    static fromJSON = (o: Serialised<Wine>) => new Wine(
        Name.fromJSON(o.name as string),
        Description.fromJSON(o.description as string),
        Year.fromJSON(o.year as number),
    )

    constructor(public readonly name: Name, public readonly description: Description, public readonly year: Year) {
        super();
        this.uuid = UUID.new();
    }
}
