import {TinyType} from 'tiny-types';

export class Response extends TinyType{

    static OK = (message: string) => new Response(200, message);
    static BAD_REQUEST = (message: string) => new Response(400, message);
    static CONFLICT = (message: string) => new Response(409, message);
    static INTERNAL_SERVER_ERROR = (message: string) => new Response(500, message);

    constructor(public readonly statusCode: number, public readonly body: string){
        super();
    }
}
