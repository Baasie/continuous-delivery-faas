import {APIGatewayEvent, Callback, Context} from 'aws-lambda';
import {DynamoDB} from 'aws-sdk';
import {Properties} from '../support/properties';
import {Response} from '../support/response';

export function handle(event: APIGatewayEvent, context: Context, callback: Callback) {
    const documentClient: DynamoDB.DocumentClient = new DynamoDB.DocumentClient();

    const scanInput: DynamoDB.DocumentClient.ScanInput = {
        TableName: Properties.WINE_TABLE_NAME,
    };

    documentClient.scan(scanInput, (error, result) => {
        if (error) {
            return callback(null, Response.INTERNAL_SERVER_ERROR(error.message));
        }
        console.log(result);
        return callback(null, Response.OK(JSON.stringify(result.Items)));
    });

}
