import {APIGatewayEvent, Callback, Context} from 'aws-lambda';
import {DynamoDB} from 'aws-sdk';
import {Wine} from '../domain/wine';
import {Properties} from '../support/properties';
import {Response} from '../support/response';
import {PutItemInputAttributeMap} from 'aws-sdk/clients/dynamodb';

export function handle(event: APIGatewayEvent, context: Context, callback: Callback) {
    const documentClient: DynamoDB.DocumentClient = new DynamoDB.DocumentClient();
    let wine: Wine;
    try {
        wine = Wine.fromJSON(JSON.parse(event.body));
    } catch (err) {
        return callback(null, Response.BAD_REQUEST(err.message));
    }
    const params = {
        TableName: Properties.WINE_TABLE_NAME,
        Item: wine.toJSON() as PutItemInputAttributeMap,
    };
    documentClient.put(params, (err, data) => {
        if (err) {
            return callback(null, Response.CONFLICT(err.message));
        }
        return callback(null, Response.OK(JSON.stringify("{ 'succes' : 'true' }")));
    });
}
