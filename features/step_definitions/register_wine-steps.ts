import {See} from '@serenity-js/core/lib/screenplay';
import {Description, Name, Wine, Year} from '../../src/domain/wine';
import {isWine, TableItems} from '../../acceptance_test/register_wine/questions/table_items';
import {RegisterWine} from '../../acceptance_test/register_wine/tasks/register_wine';

export = function deliverFundSteps() {

    this.When(/(.*) wants to register a new Wine$/, function(actorName) {
        const wine = new Wine(new Name('Shiraz'), new Description('disc'), new Year(2017));

        return this.stage.theActorCalled(actorName).attemptsTo(
            RegisterWine(wine),
        );
    });

    this.Then(/the wine should be registerd$/, function() {
        return this.stage.theActorCalled('Admin').attemptsTo(
            See.if(TableItems(), isWine('Shiraz')),
        );
    });
};
