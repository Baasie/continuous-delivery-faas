import {See} from '@serenity-js/core/lib/screenplay';
import {Wine} from '../../src/domain/wine';
import {isNotEmpty, isWine} from '../../acceptance_test/find_wines/questions/wine_response';
import {GetWines, Wines} from '../../acceptance_test/find_wines/tasks/wines';
import {LastResponse} from "@serenity-js/rest";

export = function deliverFundSteps() {

    this.Given(/several wines$/, function () {
        const wines: Wine[] = [];
        const wine = Wine.fromJSON({name: 'Merlot', description: 'disc', year: 2017});
        wines.push(wine);

        return this.stage.theActorCalled('Admin').attemptsTo(
            Wines.add(wines),
        );
    });

    this.When(/(.*) wants to get all wines$/, function (actorName) {
        return this.stage.theActorCalled(actorName).attemptsTo(
            GetWines(),
        );
    });

    this.Then(/s?he needs to get all wines$/, function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            See.if(LastResponse(), isNotEmpty()),
            See.if(LastResponse(), isWine('Merlot')),
        );
    });
};
