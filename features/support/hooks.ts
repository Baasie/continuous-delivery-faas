import {Properties} from '../../acceptance_test/properties';
import {DynamoDb} from '../../acceptance_test/support/dynamo_db';

module.exports = function() {
    this.Before(function(scenario, callback) {
        console.log('-----> before: cleaning db');
        deleteWines().then(
            () => {
                console.log('-----> before: cleanup db done....');
                callback();
        },
            err => callback(err),
        );
    });
};

function deleteWines(): PromiseLike<void> {
    const scanparams = {
        TableName: Properties.WINE_TABLE_NAME,
    };
    DynamoDb.DocumentClient().scan(scanparams, function(err, data) {
        if (err) {
            Promise.reject(err);
        } else {
            data.Items.forEach(function(wine) {
                deleteWine(wine.uuid);
            });
        }
    });
    return Promise.resolve();
}

function deleteWine(uuid: string): PromiseLike<void> {
    const delparams = {
        TableName: Properties.WINE_TABLE_NAME,
        Key: {
            uuid,
        },
    };
    DynamoDb.DocumentClient().delete(delparams, function(err, deldata) {
        if (err) {
            Promise.reject(err);
        }
    });
    return Promise.resolve();
}
